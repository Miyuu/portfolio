#pragma once

#include <vector>
#include <math.h>

enum eChildPos{
	NORTHWEST, 
	NORTHEAST, 
	SOUTHWEST, 
	SOUTHEAST
};

template <typename T>
class DSQuadTree{
public:
	DSQuadTree();
	~DSQuadTree();
#pragma region Get/Set
	int getIDNumber() const{return _idNumber;}
	int getLevel() const{return _level;}
	void getData(vector<T> &v){
        if(!_data.empty())
           v = _data;
        v.push_back(-1);
    }
#pragma endregion

	void addData(T data){_data.push_back(data);}
	void removeData(){_data.pop_back();}
	void clearData(){_data.clear();}

	DSQuadTree<T>* accessToNode(eChildPos pos);
	DSQuadTree<T>* accessToNode(int idNumber);

	void splitIt();
	void clearQuadTree();

private:
	int _idNumber;
	int _level;
	vector<T> _data;
	DSQuadTree *_childNode[4];
};

template <typename T>
DSQuadTree<T>::DSQuadTree() : _idNumber(0), _level(0){
	for (int j = 0; j < 4; j++)
		_childNode[j] = nullptr;
}

template <typename T>
DSQuadTree<T>::~DSQuadTree(){
	for(int i = 0; i < 4; i++){	
		_childNode[i]->clearData();
		delete _childNode[i];
		_childNode[i] = nullptr;
	}
}

// access node using Child position (NORTHEAST, SOUTHWEST, ...)
template <typename T>
DSQuadTree<T>* DSQuadTree<T>::accessToNode(eChildPos pos){
	if(_childNode[pos] == nullptr){
		splitIt();
	}
	return _childNode[pos];
}

// access to node using id number
template <typename T>
DSQuadTree<T>* DSQuadTree<T>::accessToNode(int idNumber){
	int id = idNumber, level = 0;

	if (idNumber - 1 < 4)	
		return _childNode[idNumber-1]; 

	while(id > 4){
		id /= 10;
		level++;
	}
	return _childNode[id - 1]->accessToNode(idNumber -id*(int)std::pow(10.0, level));	 
}

// at each access the split function is called. This function split each node in four new node
template <typename T>
void DSQuadTree<T>::splitIt(){
	for(int i = 0; i < 4; i++){
		_childNode[i] = new DSQuadTree();
		_childNode[i]->_idNumber = (_idNumber * 10 + (i+1));
		_childNode[i]->_level = _level+1;
	}
}


template <typename T>
void DSQuadTree<T>::clearQuadTree(){
	DSQuadTree<T> *node;
	int idNode;
	for(int i = 0; i < 4; i++){
		do{
			idNode = idNumber * 10 + (i+1);
			node = _childNode[i]->accessToNode(idNode);
		}while (node != nullptr);
	}
}
