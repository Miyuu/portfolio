#pragma once

class Vector
{
public:
#pragma region Ctor/Dtor
	Vector(){
		_x = 0;
		_y = 0;
		_z = 0;
	}

	Vector(double x, double y, double z = 0){
		_x = x;
		_y = y;
		_z = z;
	}

#pragma endregion

#pragma region Get/Set
	double getX() const{
		return _x;
	}

	double getY() const{
		return _y;
	}

	double getZ() const{
		return _z;
	}

	void setX(double s){
		_x = s;
	}

	void setY(double s){
		_y = s;
	}

	void setZ(double s){
		_z = s;
	}

#pragma endregion

	double Magnitude();
	void Normalize();
	void Reverse();

	Vector& operator+=(Vector v);
	Vector& operator-=(Vector v);
	Vector& operator-(Vector v);
	Vector& operator*=(double s);
	Vector& operator*(double s);
	Vector& operator/=(double s);
	Vector& operator/(double s);

	Vector CrossPdct(Vector v);

	double DotPdct(Vector v);

private:
	double	_x;
	double	_y;
	double	_z;
};