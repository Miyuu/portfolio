#pragma once

#include "DSGridNode.h"

enum eConnectionPosition{
	RIGHT,
	RIGHTDOWN,
	DOWN,
	LEFTDOWN,
	LEFT,
	LEFTUP,
	UP,
	RIGHTUP,
	NBCONNECTION};



template <typename T>
class DSGrid{
private:
	int _nNodes;
	int _sizeX;
	int _sizeY;
	DSGridNode<T> **_graph;

public:
	DSGrid(int sizeX, int sizeY);
	~DSGrid();

	int getNNode(){return _nNodes;}
	int getXSize(){return _sizeX;}
	int getYSize(){return _sizeY;}

    DSGridNode<T>* atCoordinate(int x, int y){if(x < _sizeX && y < _sizeY && x >= 0 && y >= 0) return &_graph[x][y]; return &_graph[0][0];}
};

template <typename T>
DSGrid<T>::DSGrid(int sizeX, int sizeY){
	_sizeX = sizeX;
	_sizeY = sizeY;
	_nNodes = _sizeX * _sizeY;
	_graph = new DSGridNode<T>*[_sizeX];
	for(int i = 0; i < sizeX; ++i){
		_graph[i] = new DSGridNode<T>[_sizeY];
		for(int j = 0; j < _sizeY; j++){
			_graph[i][j].setIDNumber(i + (j * _sizeY));
			_graph[i][j].setX(i);
			_graph[i][j].setY(j);
		}
	}
}

template <typename T>
DSGrid<T>::~DSGrid(){
	for(int i = 0; i < _sizeY; i++){
		delete[] _graph[i];
		_graph[i] = nullptr;
	}
	delete[] _graph;
	_graph = nullptr;
}
