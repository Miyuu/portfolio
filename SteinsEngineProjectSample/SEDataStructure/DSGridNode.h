#pragma once

#include <vector>

using namespace std;

template <typename T>
class DSGridNode{
public:
	DSGridNode(): _idNumber(0), _nConnections(0), _x(0), _y(0), _connections(nullptr){}
	~DSGridNode(){delete[] _connections; _connections = nullptr; _data.clear(); vector<T>().swap(_data);}

#pragma region Get/Set
	int getIDNumber() const{return _idNumber;}
	int getNConnections() const{return _nConnections;}
	int getX(){return _x;}
	int getY(){return _y;}
	DSGridNode<T> *getConnection(int n){return &_connections[n];}
	DSGridNode<T> *getConnection(){return _connections;}
	void getData(vector<T> &v){
        if(!_data.empty())
           v = _data;
        v.push_back(-1);
    }
	int getSizeData(){return _data.size();}

	void setIDNumber(int n){_idNumber = n;}
	void setNConnections(int n){_nConnections = n;_connections = new DSGridNode<T>[_nConnections];}
	void setConnection(int n, DSGridNode<T> *node){_connections[n] = *node;}
	void setConnection(DSGridNode<T> *node){_connections = node;}
	void setX(int x){_x = x;}
	void setY(int y){_y = y;}

#pragma endregion

	void addData(T data){_data.push_back(data);}
	void removeData(){_data.pop_back();}
	void clearData(){_data.clear();}

private:
	int				_idNumber;
	int				_nConnections;
	int				_x;
	int				_y;
	vector<T>		_data;
	DSGridNode<T>*	_connections;
};