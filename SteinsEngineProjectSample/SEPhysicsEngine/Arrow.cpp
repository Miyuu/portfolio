#include "Arrow.h"
#include "Constants.cpp"
#include <math.h>

void Arrow::launch(double angle, double range){
	_launchAngle = PI*angle/180;

	_initialVelocity = sqrt((range*(-9.81f)) / sin(2*_launchAngle));
	_acceleration.setX(0);
	_acceleration.setY(-9.81f);
}

void Arrow::update(double elapsedTime){
	_velocity.setX(_initialVelocity*cos(_launchAngle));
	_velocity.setX(_initialVelocity*sin(_launchAngle) + 9.81f*elapsedTime);
	_position.setX(_velocity.getX()*elapsedTime);
	_position.setY(_initialVelocity*sin(_launchAngle)*elapsedTime + 9.81f*pow(elapsedTime, 2));
}