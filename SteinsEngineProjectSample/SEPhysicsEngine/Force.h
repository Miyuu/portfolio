#pragma once

#include "Vector.h"

class Force
{
public:
	virtual void update() = 0;
	const Vector* getVector() const{
		return &_vector;
	}

protected:
	Vector _vector;
};