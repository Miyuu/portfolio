#pragma once

#include <d3dx11.h>
#include <xnamath.h>

class PESpriteCollisionDetection{
public:
	inline bool checkCollision(POINT sprite1Pos, SIZE sprite1Size,
							POINT sprite2Pos, SIZE sprite2Size);
};

bool PESpriteCollisionDetection::checkCollision(POINT sprite1Pos, SIZE sprite1Size,
							POINT sprite2Pos, SIZE sprite2Size){

	if(sprite1Pos.x > sprite2Pos.x + sprite2Size.cx) 
		return false;
	if(sprite1Pos.x + sprite1Size.cx < sprite2Pos.x) 
		return false;
	if(sprite1Pos.y > sprite2Pos.y + sprite2Size.cy ) 
		return false;
	if(sprite1Pos.y + sprite1Size.cy < sprite2Pos.y) 
		return false;
	return true;
}