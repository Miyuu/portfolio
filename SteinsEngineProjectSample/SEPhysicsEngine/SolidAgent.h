#pragma once

#include "ForcesHandler.h"
#include "Vector.h"

extern void update(double elapsedTime);

class SolidAgent{
public:

	SolidAgent(Vector pos = Vector(0,0,0), double mass = 0){
		_position = pos;
		_velocity = Vector();
		_acceleration = Vector();
		_mass = mass;
	}

	SolidAgent(SolidAgent& a){
		*this = a;
	}

	Vector& getPosition(){return _position;}
	Vector& getVelocity(){return _velocity;}
	double getMass(){return _mass;}
	double getRadius(){return _boundingRadius;}

	void setPosition(Vector& pos){_position = pos;}
	void setVelocity(Vector& vel){_velocity = vel;}

	void update(double elapsedTime);

protected:
	ForcesHandler		_forcesAgent;
	Vector				_position;
	Vector				_velocity;
	Vector				_acceleration;
	double				_mass;
	double				_boundingRadius; //use for bounding spheres

};