#include "PE2DPhysicAgent.h"

void PE2DPhysicAgent::init(float xPos, float yPos, float width, float height)
{
	_position.x = (LONG)xPos;
	_position.y = (LONG)yPos;
	_size.cx = (LONG)width;
	_size.cy = (LONG)height;
}

POINT PE2DPhysicAgent::getPos() const
{
	return _position;
}

SIZE PE2DPhysicAgent::getSize() const
{
	return _size;
}

bool PE2DPhysicAgent::getHit()
{
	return _hit;
}

void PE2DPhysicAgent::move(vector<void*> argv){
	int argc = argv.size();

	// Matching arguments
	switch (argc){
		case 2:
			int* t1 = static_cast<int*> (argv[0]);		
			int* t2 = static_cast<int*> (argv[1]);
			if (t1 != nullptr && t2 != nullptr){
					_position.x = (int) argv[0];
					_position.y = (int) argv[1];
			}
	}
}

void PE2DPhysicAgent::move(int x, int y){
	_position.x = x;
	_position.y = y;
	vector<void*> argv = vector<void*>();
	argv.push_back((void*)x);
	argv.push_back((void*)y);

	notify(argv);
}

void PE2DPhysicAgent::listen(vector<void*> argv){
	move(argv);
}