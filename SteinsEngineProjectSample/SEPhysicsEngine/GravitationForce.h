#pragma once

class GravitationForce: public Force
{
public :
	GravitationForce(){
		_vector = Vector(0, -9.81, 0);
	}

	void update(){};
};