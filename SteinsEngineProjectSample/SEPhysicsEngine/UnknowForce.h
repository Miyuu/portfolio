#pragma once

class UnknownForce: public Force
{
public :
	UnknownForce(){
		_vector = new Vector(0, 0, 0);
	}

	void update(){};
};