#include "SolidAgent.h"

void SolidAgent::update(double elapsedTime){
	_forcesAgent.updateForces();
	Vector force = _forcesAgent.getCombinedForce();
	
	// update data
	_acceleration = force / _mass;
	_velocity += _acceleration * elapsedTime; //v = u + at
	_position += (_velocity * elapsedTime) - (_acceleration * elapsedTime * 0.5f);   //s = vt - 1/2*at
}