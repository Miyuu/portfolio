#pragma once

#include "ILaunchable.h"
#include "SolidAgent.h"
#include "GravitationForce.h"

class Arrow: public ILaunchable, public SolidAgent
{
public:
	Arrow(Vector pos = Vector(0,0,0), double mass = 0){
		_position = pos;
		_velocity = Vector();
		_acceleration = Vector();
		_mass = mass;
		_launchAngle = 0;
	}

	Arrow(Arrow& a){
		*this = a;
	}

	void launch(double angle = 90, double range = 50);
	void update(double elapsedTime);

private:
	double	_launchAngle; //rad
	double	_initialVelocity;
	double	_range; //px
};