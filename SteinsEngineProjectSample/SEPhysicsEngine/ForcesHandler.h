#pragma once

#include <list>

#include "Force.h"

class ForcesHandler
{
public:
	void updateForces(){
		std::list<Force*>::iterator it = _forces.begin();
		std::list<Force*>::iterator end = _forces.end();
		for (; it != end; ++it){
			(*it)->update();
		}
	}

	void addForce(Force& f){
		_forces.push_back(&f);
	}

	Vector getCombinedForce(){
		Vector combined = Vector();
		std::list<Force*>::iterator it = _forces.begin();
		std::list<Force*>::iterator end = _forces.end();
		for (; it != end; ++it){
			combined += *((*it)->getVector());
		}
		return combined;
	}

private:
	std::list<Force*> _forces;
};