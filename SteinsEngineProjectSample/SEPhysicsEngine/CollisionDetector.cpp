#include "Vector"
#include "CollisionDetector.h"

#define C_TOLERANCE		2.0f
#define	C_RESTITUTION	0.5f

Vector	vCollisionNormal;
Vector	vRelativeVelocity;

using namespace std;
int CollisionDetector::checkForCollision(SolidAgent b1, SolidAgent b2){
	Vector d;
	double r;
	double s;
	Vector v1, v2;
	double vrn;

	r = b1.getRadius() / 2 + b2.getRadius() / 2;
	d = b1.getPosition() - b2.getPosition();
	s = d.Magnitude() - r;

	d.Normalize();
	vCollisionNormal = d;

	v1 = b1.getVelocity();
	v2 = b2.getVelocity();
	vRelativeVelocity = v1 - v2;
	
	vrn = vRelativeVelocity.DotPdct(vCollisionNormal);
	if((fabs(s) <= C_TOLERANCE) && (vrn > 0.0f))
		return 1; //collision
	else if(s < -C_TOLERANCE)
		return -1; //interpenetrating
	else
		return 0; //no collision
}

void CollisionDetector::applyCollisionImpulse(SolidAgent b1, SolidAgent b2){
	double j = (-(1+C_RESTITUTION) * (vRelativeVelocity.DotPdct(vCollisionNormal)))/
		((vCollisionNormal.DotPdct(vCollisionNormal)) *
		(1/b1.getMass() + 1/b2.getMass()));

	b1.setVelocity(b1.getVelocity() += (vCollisionNormal * j) / b1.getMass());
	b2.setVelocity(b2.getVelocity() += (vCollisionNormal * j) / b2.getMass());
}

list<pair<SolidAgent*, SolidAgent*> > CollisionDetector::simulation(double elapsedTime, list<SolidAgent*> l){
	int check = 0;
	list<pair<SolidAgent*, SolidAgent*> > result;

	list<SolidAgent*>::iterator it = l.begin();
	list<SolidAgent*>::iterator end = l.end();
	list<SolidAgent*>::iterator it2 = l.begin();
	
	for(; it != end; ++it){
		for(it2 = it; it2 != end; ++it2){
			check = checkForCollision(*(*it), *(*it2));

			switch (check){
				case -1:
					applyCollisionImpulse(*(*it), *(*it2));
					result.push_back(pair<SolidAgent*, SolidAgent*>(*it, *it2));

				case 1: 
					applyCollisionImpulse(*(*it), *(*it2));
					result.push_back(pair<SolidAgent*, SolidAgent*>(*it, *it2));
			}
		}
	}
	return result;
}