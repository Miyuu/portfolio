#pragma once

class ILaunchable
{
public:
	virtual void launch();
	virtual void update();
};