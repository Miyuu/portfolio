#pragma once

#include "GOIMoveable.h"
#include "GEIObserver.h"
#include "GEGameObject.h"
#include <d3dx11.h>
#include <xnamath.h>

class PE2DPhysicAgent: public GOIMoveable, public GEIObserver, public GEGameObject{
public:
	PE2DPhysicAgent() : _hit(false){}
	~PE2DPhysicAgent(){}

	void init(float xPos, float yPos, float width, float height);

	POINT getPos() const;
	SIZE getSize() const;
	bool getHit();
	bool getIsAlly(){return _isAlly;};
	string getName(){return _name;}

	void setPos(int x, int y){_position.x = (LONG)x; _position.y = (LONG)y;}
	void setSize(int width, int height){_size.cx =(LONG)width; _size.cy = (LONG)height;}
	void setHit(bool hit){_hit = hit;}
	void setName(string name){_name = name;}
	void setIsAlly(bool isAlly){_isAlly = isAlly;}

	inline bool checkCollision(PE2DPhysicAgent* agent);

	void move(int x, int y);
	void move(vector<void*> argv);
	void listen(vector<void*> argv);

private:
	POINT	_position;
	SIZE	_size;
	string _name;
	bool _hit;
	bool _isAlly;
};

bool PE2DPhysicAgent::checkCollision(PE2DPhysicAgent* agent){
		// if lui plus grand or lui est plus grand
	if((agent->_position.x >= this->_position.x) && (agent->_position.x <= (this->_position.x + this->_size.cx)) 
		|| (agent->_position.x + agent->_size.cx >= this->_position.x) && (agent->_position.x + agent->_size.cx <= (this->_position.x + this->_size.cx))){
		if((agent->_position.y >= this->_position.y) && (agent->_position.y <= (this->_position.y + this->_size.cy))
			|| (agent->_position.y + agent->_size.cy >= this->_position.y) && (agent->_position.y + agent->_size.cy  <= (this->_position.y + this->_size.cy))){
			agent->setHit(true);
			return true;
		}
	}
	
	if((this->_position.x <= (agent->_position.x + agent->_size.cx)) && (this->_position.x >= agent->_position.x)
		|| (this->_position.x + this->_size.cx <= (agent->_position.x + agent->_size.cx)) && (this->_position.x + this->_size.cx >= agent->_position.x)){
		if(this->_position.y <= (agent->_position.y + agent->_size.cy) && (this->_position.y >= agent->_position.y)
			|| this->_position.y + this->_size.cy <= (agent->_position.y + agent->_size.cy) && (this->_position.y  + this->_size.cy >= agent->_position.y)){
			agent->setHit(true);
			return true;
		}
	}
	return false;
}