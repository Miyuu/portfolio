#pragma once

#include "SolidAgent.h"
#include <list>

using namespace std;
class CollisionDetector{

public:
	int checkForCollision(SolidAgent b1, SolidAgent b2);
	void applyCollisionImpulse(SolidAgent b1, SolidAgent b2);
	
	list<pair<SolidAgent*, SolidAgent*> > simulation(double elapsedTime, list<SolidAgent*> list);

};