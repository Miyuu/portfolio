#pragma once

#include <vector>
#include <map>
#include <iostream>

#include "GEIObservable.h"
#include "GEIObserver.h"

using namespace std;

class GEMessageCenter{
public:
	static GEMessageCenter* getInstance(){
		if (_instance == nullptr)
			_instance = new GEMessageCenter();
		return _instance;
	}
	
	void subscribe(GEIObservable* func, GEIObserver* subscriber);
	bool dispatch(GEIObservable* observedItem, vector<void*> argv);
		
private:
	GEMessageCenter(){}
	~GEMessageCenter(){
		_subscriptions.clear();
		delete _instance;
		_instance = nullptr;
	}

private:
	map<GEIObservable*, vector<GEIObserver*> >	_subscriptions;
	static GEMessageCenter*						_instance;
};