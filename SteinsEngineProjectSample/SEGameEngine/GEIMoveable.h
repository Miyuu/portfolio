#pragma once
#include "GEIObservable.h"
#include "GEMessageCenter.h"

class GEIMoveable: public GEIObservable{
public:
	virtual void notify(vector<void*> argv){
		GEMessageCenter* gMessageCenter = GEMessageCenter::getInstance();
		gMessageCenter->dispatch(this, argv);
	}

	virtual void move(vector<void*> argv) = 0;
};