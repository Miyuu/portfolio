#pragma once

#include <windows.h>

class Timer {
 
public:
    Timer() {
      reset();
    }
   
    void reset() {
      unsigned __int64 pf;
      QueryPerformanceFrequency( (LARGE_INTEGER *)&pf );
      _frequency = 1.0 / (double)pf;
      QueryPerformanceCounter( (LARGE_INTEGER *)&_baseTime );
    }
  
    double elapsedTimeSec() {
      unsigned __int64 elapsed;
      QueryPerformanceCounter( (LARGE_INTEGER *)&elapsed );
      return (elapsed - _baseTime) * _frequency;
    }

    double elapsedTimeMSec() {
      return elapsedTimeSec() * 1000.0;
    }
  private:
    double				_frequency;
    unsigned __int64	_baseTime;
};

