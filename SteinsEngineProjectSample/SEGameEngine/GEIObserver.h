#pragma once
#include <vector>
#include <string>

class GEIObserver{
public:
	virtual void listen(vector<void*> argv) = 0;
};