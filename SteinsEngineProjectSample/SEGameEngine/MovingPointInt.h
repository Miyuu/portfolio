#pragma once

#include "GEIMoveable.h"
#include "GEIObserver.h"
#include "GEGameObject.h"

class MovingPointInt: public GEIMoveable, public GEIObserver, public GEGameObject{
public:
	MovingPointInt(int x, int y){
		_x = x;
		_y = y;
	}

	int getX(){return _x;}
	int getY(){return _y;}

	void move(int x, int y);
	void move(vector<void*> argv);
	void listen(vector<void*> argv);

private:
	int _x;
	int _y;
};