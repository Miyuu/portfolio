#pragma once
#include "GEIObservable.h"
#include "GEMessageCenter.h"

class GOIUpdatable: public GEIObservable{
public:
	virtual void notify(vector<void*> argv){
		GEMessageCenter* gMessageCenter = GEMessageCenter::getInstance();
		gMessageCenter->dispatch(this, argv);
	}

	virtual void update(vector<void*> argv) = 0;
};