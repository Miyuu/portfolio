#pragma once

#include "GOIMoveable.h"

//TODO: change int for Vector class

class GOCPositionComponent: public GOIMoveable, public GEIObserver{
public:
	GOCPositionComponent(int x, int y, int z){
		_x = x;
		_y = y;
		_z = z;
	}
	GOCPositionComponent(int x, int y){
		_x = x;
		_y = y;
		_z = 0;
	}
	GOCPositionComponent(){
		_x = 0;
		_y = 0;
		_z = 0;
	}


	void listen(vector<void*> argv);
	void move(vector<void*> argv);

	int getX(){return _x;}
	int getY(){return _y;}
	int getZ(){return _z;}

	void move(int x, int y);
	void move(int x, int y, int z);

private:
	int _x, _y, _z;	
};