#pragma once

#include "GEGameObject.h"

#include <string>
#include <vector>
using namespace std;

class GEIObservable{
public:
	virtual void notify(vector<void*> argv) = 0;

	GEGameObject*	getGOPointer(){return this->_GOPointer;}

private:
	GEGameObject*	_GOPointer;
};