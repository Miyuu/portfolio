#include "GEHealthComponent.h"

void GEHealthComponent::update(vector<void*> argv){
	int argc = argv.size();

	// Matching arguments
	switch (argc){
		case 2:
			int* t1 = static_cast<int*> (argv[0]);		//Use dynamic_cast when possible
			int* t2 = static_cast<int*> (argv[0]);
			if (t1 != nullptr && t2 != nullptr){
				if (*t2 == 0){				//Health reset
					_health = *t1;
					return;
				}
				if (*t2 == 1){
					_health += *t1;	//Health increase
					return;
				}
				if (*t2 == -1){
					_health -= *t1;	//Health decrease
					return;
				}
			}
	}
}

void GEHealthComponent::setHealth(int health){
	_health = health;
	vector<void*> argv = vector<void*>();
	argv.push_back((void*)health);
	argv.push_back((void*)0);

	notify(argv);
}

void GEHealthComponent::increaseHealth(int f){
	_health += f;
	vector<void*> argv = vector<void*>();
	argv.push_back((void*)f);
	argv.push_back((void*)1);

	notify(argv);
}

void GEHealthComponent::decreaseHealth(int f){
	_health -= f;
	vector<void*> argv = vector<void*>();
	argv.push_back((void*)f);
	argv.push_back((void*)-1);

	notify(argv);
}

void GEHealthComponent::listen(vector<void*> argv){
	update(argv);
}