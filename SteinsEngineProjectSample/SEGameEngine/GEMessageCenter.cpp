#include "GEMessageCenter.h"

GEMessageCenter* GEMessageCenter::_instance = nullptr;

void GEMessageCenter::subscribe(GEIObservable* func, GEIObserver* subscriber){
	map<GEIObservable*, vector<GEIObserver*> >::iterator it;
	it = _subscriptions.find(func);
		
	if (it != _subscriptions.end()){
		it->second.push_back(subscriber);
	}
	else{
		vector<GEIObserver*> v = vector<GEIObserver*>();
		v.push_back(subscriber);
		_subscriptions.insert(pair<GEIObservable*, vector<GEIObserver*>>(func, v));
	}
}

bool GEMessageCenter::dispatch(GEIObservable* observedItem, vector<void*> argv){
	map<GEIObservable*, vector<GEIObserver*> >::iterator it;
	it = _subscriptions.find(observedItem);

	if (it != _subscriptions.end()){
		vector<GEIObserver*>::iterator itv = it->second.begin();
		vector<GEIObserver*>::iterator endv = it->second.end();

		for(; itv != endv; ++itv){
			(*itv)->listen(argv);
		}
		return true;
	}
	return false;
}