#pragma once

#include "GOIUpdatable.h"

class GOCHealthComponent: public GOIUpdatable, public GEIObserver{
public:
	void update(vector<void*> argv);
	void listen(vector<void*> argv);

	void setHealth(int health);
	void increaseHealth(int f);
	void decreaseHealth(int f);

private:
	int _health;
};