#include "GOCPositionComponent.h"

#include <vector>

void GOCPositionComponent::move(vector<void*> argv){
	int argc = argv.size();

	int* t1 = nullptr;
	int* t2 = nullptr;
	// Matching arguments
	switch (argc){
		case 2:
			t1 = static_cast<int*> (argv[0]);		
			t2 = static_cast<int*> (argv[1]);
			if (t1 != nullptr && t2 != nullptr){
				_x = (int) *t1;
				_y = (int) *t2;
			}
			break;
		case 3:
			t1 = static_cast<int*> (argv[0]);		
			t2 = static_cast<int*> (argv[1]);
			int* t3 = static_cast<int*> (argv[1]);
			if (t1 != nullptr && t2 != nullptr && t3 != nullptr){
				_x = (int) *t1;
				_y = (int) *t2;
				_z = (int) *t3;
			}
			break;
	}
}

void GOCPositionComponent::move(int x, int y){
	_x = x;
	_y = y;
	vector<void*> argv = vector<void*>();
	argv.push_back((void*)x);
	argv.push_back((void*)y);

	notify(argv);
}

void GOCPositionComponent::move(int x, int y, int z){
	_x = x;
	_y = y;
	_z = z;
	vector<void*> argv = vector<void*>();
	argv.push_back((void*)x);
	argv.push_back((void*)y);
	argv.push_back((void*)z);

	notify(argv);
}

void GOCPositionComponent::listen(vector<void*> argv){
	move(argv);
}