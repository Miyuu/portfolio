#pragma once

#include "GEIUpdatable.h"

class GEHealthComponent: GEIUpdatable, public GEIObserver{
public:
	void update(vector<void*> argv);
	void GEHealthComponent::listen(vector<void*> argv);

	void setHealth(int health);
	void increaseHealth(int f);
	void decreaseHealth(int f);

private:
	int _health;
};