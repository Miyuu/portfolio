#pragma once

#include "BehaviourTree.h"

using namespace std;
class BTActionRunAway: public BehaviourTree{

public:
	bool execute(SingleAgent* agent){
		agent->changeState(RunAway);

		cout << "RunAway" << endl;
		return true;
	}
};