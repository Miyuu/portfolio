#pragma once

#include "BehaviourTree.h"

using namespace std;
class BTConditionHPCritical: public BehaviourTree{

public:
	bool execute(SingleAgent* agent){
		return (agent->getHPRate() < 0.2f);
	}
};