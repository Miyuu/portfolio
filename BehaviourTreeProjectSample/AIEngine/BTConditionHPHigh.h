#pragma once

#include "BehaviourTree.h"

using namespace std;
class BTConditionHPHigh: public BehaviourTree{

public:
	bool execute(SingleAgent* agent){
		// TODO: strategy driven
		return (agent->getHPRate() >= 0.5f);
	}
};