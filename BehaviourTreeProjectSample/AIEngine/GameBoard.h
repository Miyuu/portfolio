#pragma once

#include <vector>
#include <iostream>
#include "Tile.h"
#include "Utils.h"
#include "Player.h"

using namespace std;
class GameBoard{

public:
	static GameBoard* getInstance(){
		if (_boardInstance == nullptr)
			_boardInstance = new GameBoard();
		return _boardInstance;
	}

	void initSquads(Player p, vector<SingleAgent*> a);

	void draw();

	Vector isEnemyNearby(int i, int j);

	void makeMove(Vector pos);

	Vector chooseNewPosFw(int i, int j, Player p);
	Vector chooseNewPosBw(int i, int j, Player p);

private:
	GameBoard(){
		_board = vector<vector<Tile> >(HSIZE, vector<Tile>(WSIZE,Tile()));
		for(int i=0; i<HSIZE; ++i)
			for(int j=0; j<WSIZE; ++j){
				_board[i][j] = Tile();
			}
	}

private:
	static GameBoard*		_boardInstance;
	vector<vector<Tile> >	_board;
};