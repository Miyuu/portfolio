#pragma once

enum eState{
	Dead=0,
	Wait,
	RunAway,
	Chase,
	Attack,
	Defend
};