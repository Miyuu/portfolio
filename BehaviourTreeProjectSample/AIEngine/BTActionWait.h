#pragma once

#include "BehaviourTree.h"

using namespace std;
class BTActionWait: public BehaviourTree{

public:
	bool execute(SingleAgent* agent){
		agent->changeState(Wait);

		cout << "Wait" << endl;
		return true;
	}
};