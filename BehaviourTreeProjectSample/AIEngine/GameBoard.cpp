#include "GameBoard.h"
#include <random>
#include  <time.h>

GameBoard* GameBoard::_boardInstance = nullptr;

Vector GameBoard::chooseNewPosFw(int i, int j, Player p){
	srand((int)time(nullptr) );
	int ri = rand()%(WSIZE-1);
	int rj = rand()%(HSIZE-1);
	int ti = i;
	int tj = j;


	while (true){
		if(ri<i)
			ti = i - 1;
		else if(ri>i)
			ti = i + 1;

		if(rj<j)
			tj = j - 1;
		else if(rj>j)
			tj = j + 1;
	
		if ((ti < WSIZE) && (ti >=0) &&
			(tj < HSIZE) && (tj >=0) &&
			(_board[ti][tj].getContent() != nullptr))
		{
			ri = rand()%WSIZE;
			rj = rand()%HSIZE;
		}
		else
			return Vector(ti, tj, 0);
	}
}

Vector GameBoard::chooseNewPosBw(int i, int j, Player p){
	srand((int)time(nullptr) );
	int ri = rand()%(WSIZE-1);
	int rj = rand()%(HSIZE-1);
	int ti = i;
	int tj = j;


	while (true){
		if(ri<i)
			ti = i + 1;
		else if(ri>i)
			ti = i - 1;

		if(rj<j)
			tj = j + 1;
		else if(rj>j)
			tj = j - 1;
	
		if ((ti < WSIZE) && (ti >=0) &&
			(tj < HSIZE) && (tj >=0) &&
			(_board[ti][tj].getContent() != nullptr))
		{
			ri = rand()%WSIZE;
			rj = rand()%HSIZE;
		}
		else
			return Vector(ti, tj, 0);
	}
}


void GameBoard::makeMove(Vector pos){
		SingleAgent* a = _board[(int)pos.getX()][(int)pos.getY()].getContent();

		SingleAgent* b = nullptr;
		int i = (int)pos.getX();
		int j = (int)pos.getY();
		Vector v;

		switch (a->getState()){
			case Attack:
				v = a->getNearestFoe();
				b = _board[(int)v.getX()][(int)v.getY()].getContent();
				if(b->getState() == Defend)
					b->decreaseHP(1);
				else
					b->decreaseHP(2);
				break;
			
			case Chase:
				v = this->chooseNewPosFw((int)pos.getX(), (int)pos.getY(), a->getPlayer());
				_board[(int)v.getX()][(int)v.getY()].setContent(a);
				_board[i][j].setContent(nullptr);
				a->moveAgent(v);
				break;

			case RunAway:
				v = this->chooseNewPosBw((int)pos.getX(), (int)pos.getY(), a->getPlayer());
				_board[(int)v.getX()][(int)v.getY()].setContent(a);
				_board[i][j].setContent(nullptr);
				a->moveAgent(v);
				break;

			case Dead:
				_board[i][j].setContent(nullptr);
		}
	}

void GameBoard::draw(){
		for(int i=0; i<HSIZE; ++i){
			cout << "|";
			for(int j=0; j<WSIZE; ++j){
				if (_board[i][j].getContent() == nullptr)
					cout << "  ";
				else if (_board[i][j].getContent()->getPlayer() == Player1)
					cout << "A1";
				else
					cout << "A2";
				cout << "|";
			}
			cout << endl;
		}
		cout << endl;
	}

Vector GameBoard::isEnemyNearby(int i, int j){
	if (_board[i][j].getContent() == nullptr)
		return Vector(-1, -1);

	Player player = _board[i][j].getContent()->getPlayer();

	if((i+1 < WSIZE) && _board[i+1][j].getContent() &&
		(_board[i+1][j].getContent()->getPlayer() != player))
		return Vector(i+1, j);
	
	if((j+1 < HSIZE) && _board[i][j+1].getContent() &&
		(_board[i][j+1].getContent()->getPlayer() != player))
		return Vector(i, j+1);
		
	if((i > 0) && _board[i-1][j].getContent() &&
		(_board[i-1][j].getContent()->getPlayer() != player))
		return Vector(i-1, j);
		
	if((j > 0) && _board[i][j-1].getContent() &&
		(_board[i][j-1].getContent()->getPlayer() != player))
		return Vector(i, j-1);

	return Vector(-1,-1);
	}

void GameBoard::initSquads(Player p, vector<SingleAgent*> a){
		int nbUnits = a.size();

		if (p == Player1){
			for (int i=0; i<nbUnits; ++i){
				_board[i/WSIZE][i%WSIZE].setContent(a[i]);
			}
		}
		else if (p == Player2){
			for (int i=0; i<nbUnits; ++i){
				_board[WSIZE- i/WSIZE-1][WSIZE - i%WSIZE -1].setContent(a[i]);
			}
		}
	}