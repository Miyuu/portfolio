#pragma once

#include "BehaviourTree.h"

using namespace std;
class BTSelector: public BehaviourTree{

public:
	BTSelector(){_children = list<BehaviourTree*>();}
	virtual ~BTSelector(){cleanup();}
	
	void addChild(BehaviourTree* c){_children.push_back(c);}

	virtual void cleanup(){
		auto it = _children.begin();
		auto end = _children.end();

		for(; it != end; ++it){
			if(*it){
				(*it)->cleanup();
				delete (*it);
				*it = nullptr;
			}
		}
	}

	virtual bool execute(SingleAgent* agent){
		auto it = _children.begin();
		auto end = _children.end();
		bool res = false;
		while ((it != end) && !res){
			res = (*it)->execute(agent);
			++it;
		}
		return res;
	}

	
protected:
	list<BehaviourTree*> _children;	
};