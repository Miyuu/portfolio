#pragma once

#include "Vector.h"
#include "Strategy.h"
#include "State.h"
#include "Player.h"

class SingleAgent{

public:
	SingleAgent(Player p, Vector pos = Vector(0,0,0)){
		_strategy = Neutral;
		_maxHP = 10;
		_currentHP = _maxHP;
		_position = pos;
		_currentState = Wait;
		_player = p;
		_nearestFoe = Vector(-1, -1);
	}

	void changeStrategy(eStrategy s){_strategy = s;}

	void moveAgent(Vector& newPos){_position = newPos;}

	void changeState(eState s){_currentState = s;}

	void increaseHP(int n){
		if ((_currentHP + n) < _maxHP)
			_currentHP += n;
		else
			_currentHP = _maxHP;
	}

	void decreaseHP(int n){
		if ((_currentHP - n) > 0)
			_currentHP -= n;
		else
			_currentHP = 0;
	}

	eState getState(){return _currentState;}

	int getHP(){return _currentHP;}

	float getHPRate(){return (float)_currentHP / (float)_maxHP;}

	Vector getPosition(){return _position;}

	Player getPlayer(){return _player;}

	Vector getNearestFoe(){return _nearestFoe;}
	void setNearestFoe(Vector v){_nearestFoe = v;}

private:
	eStrategy			_strategy;
	eState				_currentState;
	Player				_player;

	Vector				_nearestFoe;

	//environment
	Vector			_position;
	int		 		_maxHP;
	int				_currentHP;
};
