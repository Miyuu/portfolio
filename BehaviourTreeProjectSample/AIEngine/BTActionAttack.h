#pragma once

#include "BehaviourTree.h"

using namespace std;
class BTActionAttack: public BehaviourTree{

public:
	bool execute(SingleAgent* agent){
		agent->changeState(Attack);
		cout << "Attack" << endl;

		return true;
	}
};