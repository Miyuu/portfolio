#pragma once

#include "SingleAgent.h"

class Tile{
public:
	Tile(){_content = nullptr;}

	void setContent(SingleAgent* a){ _content = a;}
	SingleAgent* getContent(){return _content;}

private:
	SingleAgent*	_content;
};