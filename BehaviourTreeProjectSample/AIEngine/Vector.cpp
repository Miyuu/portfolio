#include "Vector.h"

#include <math.h>

#define TOL 0.001f

double Vector::Magnitude(){
	return sqrt(pow(_x, 2)  + pow(_y, 2));
}

void Vector::Normalize(){
	double m = sqrt(pow(_x, 2)  + pow(_y, 2));
	if (m <= TOL)
		m = 1;
	_x /= m;
	_y /= m;
	_z /= m;

	if (fabs(_x) < TOL)
		_x = 0.0f;
	if (fabs(_y) < TOL)
		_y = 0.0f;
	if (fabs(_z) < TOL)
		_z = 0.0f;
}

void Vector::Reverse(){
	_x = -_x;
	_y = -_y;
	_z = -_z;
}

Vector& Vector::operator+=(Vector v){
	_x += v.getX();
	_y += v.getY();
	_z += v.getZ();
	return *this;
}

Vector& Vector::operator-=(Vector v){
	_x -= v.getX();
	_y -= v.getY();
	_z -= v.getZ();
	return *this;
}

Vector& Vector::operator-(Vector v){
	return Vector(
				_x - v.getX(),
				_y - v.getY(),
				_z - v.getZ());
}

Vector& Vector::operator*=(double s){
	_x *= s;
	_y *= s;
	_z *= s;
	return *this;
}

Vector& Vector::operator*(double s){
	return Vector(
				_x * s,
				_y * s,
				_z * s);
}


Vector& Vector::operator/=(double s){
	_x /= s;
	_y /= s;
	_z /= s;
	return *this;
}

Vector& Vector::operator/(double s){
	return Vector(
				_x / s,
				_y / s,
				_z / s);
}

Vector Vector::CrossPdct(Vector v){
	return Vector(
		_y*v.getZ() - _z*v.getY(),
		_x*v.getZ() - _z*v.getX(),
		_x*v.getY() - _y*v.getX());
}

double Vector::DotPdct(Vector v){
	return (_x*v.getX() + _y*v.getY() + _z*v.getZ());
}
