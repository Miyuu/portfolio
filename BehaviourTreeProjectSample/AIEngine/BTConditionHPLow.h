#pragma once

#include "BehaviourTree.h"

using namespace std;
class BTConditionHPLow: public BehaviourTree{

public:
	bool execute(SingleAgent& agent, GameBoard* gameStatus){
		return ((agent.getHPRate() < 0.5f) && (agent.getHPRate() >= 0.2f));
	}
};