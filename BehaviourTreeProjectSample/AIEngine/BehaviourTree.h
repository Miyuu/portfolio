#pragma once
#include <list>
#include <vector>
#include <iostream>
#include "SingleAgent.h"

using namespace std;
class BehaviourTree{

public:
	BehaviourTree(){}

	virtual void cleanup(){};

	virtual bool execute(SingleAgent* agent) = 0;
};