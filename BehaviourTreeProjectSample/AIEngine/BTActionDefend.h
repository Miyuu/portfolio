#pragma once

#include "BehaviourTree.h"

using namespace std;
class BTActionDefend: public BehaviourTree{

public:
	bool execute(SingleAgent* agent){
		agent->changeState(Defend);
		
		cout << "Defend" << endl;
		return true;
	}
};