// This is the main DLL file.

#include "GameBoard.h"
#include "BTSingleAgentDecisionMaker.h"
#include "CPU.h"

#define DEBUG 

bool gGameEnded;

bool play(CPU player, BehaviourTree* behavior){
	int i = player.getSquadSize();
	bool hr = true;
	GameBoard* board = GameBoard::getInstance();

	for(int j=0; j<i; ++j){
		hr = behavior->execute(player.getUnit(j));
		if(player.getUnit(j)->getState() == Dead){
			player.removeUnit(j);
			--i;
		}
		board->makeMove(player.getUnit(j)->getPosition());
	}
	return player.hasLost();
}

int main(int argc, char** argv)
{
	GameBoard* board = GameBoard::getInstance();
	
	CPU p1 = CPU(Player1, 2);
	board->initSquads(p1.getPlayer(), p1.getSquad());
	CPU p2 = CPU(Player2, 2);
	board->initSquads(p2.getPlayer(), p2.getSquad());

	BTSingleAgentDecisionMaker* dm = new BTSingleAgentDecisionMaker();
	dm->init();

	gGameEnded = false;

	board->draw();

#ifdef DEBUG
	int i = 8;
#endif

	while (!gGameEnded)
	{
	#ifdef DEBUG
		if(i <= 0)
			break;
	#endif

		std::cout << "Player 1" << std::endl;
		gGameEnded = play(p1, dm);
		std::cout << std::endl;
		if(gGameEnded){
			std::cout << "Player 2 WINS!!" << std::endl;
			break;
		}

		std::cout << "Player 2" << std::endl;
		gGameEnded = play(p2, dm);
		std::cout << std::endl;
		if(gGameEnded){
			std::cout << "Player 1 WINS!!" << std::endl;
			break;
		}

		board->draw();
	#ifdef DEBUG
		--i;	
	#endif
	}

	System::Console::ReadLine();
	dm->cleanup();
	return 0;
}