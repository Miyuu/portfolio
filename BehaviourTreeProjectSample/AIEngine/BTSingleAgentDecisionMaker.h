#pragma once

#include "BTActionAttack.h"
#include "BTActionChase.h"
#include "BTActionDefend.h"
#include "BTActionDie.h"
#include "BTActionRunAway.h"
#include "BTActionWait.h"
#include "BTConditionEnemyNearby.h"
#include "BTConditionHPCritical.h"
#include "BTConditionHPHigh.h"
#include "BTConditionHPLow.h"
#include "BTConditionHPNull.h"
#include "BTSelector.h"
#include "BTSequential.h"

class BTSingleAgentDecisionMaker: public BTSelector{

public:

	BTSingleAgentDecisionMaker(){_children = list<BehaviourTree*>();}
	
	void init(){
		BTSequential* s1 = new BTSequential();
		s1->addChild(new BTConditionHPNull());
		s1->addChild(new BTActionDie());
		_children.push_back(s1);

		BTSequential* s2 = new BTSequential();
		s2->addChild(new BTConditionHPHigh());
		BTSelector* s21 = new BTSelector();
		BTSequential* s211 = new BTSequential();
		s211->addChild(new BTConditionEnemyNearby());
		s211->addChild(new BTActionAttack());
		s21->addChild(s211);
		s21->addChild(new BTActionChase());
		s2->addChild(s21);
		_children.push_back(s2);

		BTSequential* s3 = new BTSequential();
		s3->addChild(new BTConditionEnemyNearby());
		BTSelector* s31 = new BTSelector();
		BTSequential* s311 = new BTSequential();
		s311->addChild(new BTConditionHPCritical());
		s311->addChild(new BTActionRunAway());
		s31->addChild(s311);
		s31->addChild(new BTActionDefend());
		_children.push_back(s3);

		_children.push_back(new BTActionWait());
	};

	void exit(){cleanup();}
};