#pragma once

#include "BehaviourTree.h"
#include "GameBoard.h"

using namespace std;
class BTConditionEnemyNearby: public BehaviourTree{

public:
	bool execute(SingleAgent* agent){
		GameBoard* board = GameBoard::getInstance();
		Vector v = board->isEnemyNearby(agent->getPosition().getX(), agent->getPosition().getY());
		agent->setNearestFoe(v);
		if (v.getX() != -1)
			return true;

		return false;
	}
};