#pragma once

#include "UnitBehaviorTree.h"

class OffensiveBehaviorTree: public UnitBehaviorTree{

public:
 	 eState makeDecision(Agent& agent, Map& gameStatus);
};