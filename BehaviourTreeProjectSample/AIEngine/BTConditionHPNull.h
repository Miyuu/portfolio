#pragma once

#include "BehaviourTree.h"

using namespace std;
class BTConditionHPNull: public BehaviourTree{

public:
	bool execute(SingleAgent* agent){
		// TODO: strategy driven
		return (agent->getHPRate() == 0.0f);
	}
};