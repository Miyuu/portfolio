#pragma once

#include "BehaviourTree.h"

using namespace std;
class BTActionDie: public BehaviourTree{

public:
	bool execute(SingleAgent* agent){
		agent->changeState(Dead);

		cout << "Dead" << endl;
		return true;
	}
};