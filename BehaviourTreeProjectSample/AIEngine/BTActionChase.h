#pragma once

#include "BehaviourTree.h"

using namespace std;
class BTActionChase: public BehaviourTree{

public:
	bool execute(SingleAgent* agent){
		agent->changeState(Chase);

		cout << "Chase" << endl;
		return true;
	}
};