#pragma once
#include <vector>
#include "SingleAgent.h"
#include "Utils.h"

using namespace std;
class CPU{
public:
	CPU(Player p, int nUnit){
		_player = p;

		if (p == Player1)
			for (int i=0; i<nUnit; ++i){
				SingleAgent* unit = new SingleAgent(Player1, Vector(i/WSIZE, i%WSIZE, 0));
				_squad.push_back(unit);
			}
		if (p == Player2)
			for (int i=0; i<nUnit; ++i){
				SingleAgent* unit = new SingleAgent(Player2, Vector(WSIZE- i/WSIZE -1, WSIZE - i%WSIZE -1));
				_squad.push_back(unit);
			}
	}

	void addUnit(SingleAgent* u){_squad.push_back(u);}
	
	void removeUnit(int p){_squad.erase(_squad.begin()+p);}

	bool hasLost(){return _squad.size() <= 0;}

	SingleAgent* getUnit(int i){
		if(i<_squad.size())
			return _squad[i];
	}

	vector<SingleAgent*> getSquad(){return _squad;}

	int getSquadSize(){return _squad.size();}
	Player getPlayer(){return _player;}

private:
	vector<SingleAgent*>	_squad;
	Player					_player;
};