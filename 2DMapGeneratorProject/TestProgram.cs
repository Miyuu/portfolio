﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MapGeneratorLibrary;

namespace MapGeneratorLibrary
{
    class TestProgram
    {
        static void Main(string[] args)
        {
            RandomMap map = RandomMap.GenerateMap(GroundType.ROCKY, ObstacleQuantity.LESS, new Pair(10, 10));
            map.DisplayServerMap();
            Console.Read();
        }
    }
}
