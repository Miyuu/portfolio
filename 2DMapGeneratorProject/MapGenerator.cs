using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBO;
using DBO.Transfer;

namespace MapGeneratorLibrary
{

    public enum Landscape
    {
        FOREST,
        PLAIN,
        WASTELAND,
        DESERT
    }

    public enum GroundType
    {
        WOODY, /*Allen.*/
        ROCKY, /*Balboa.*/
        WATERY, /* Eeeer...*/
        BALANCED
    }

    public enum ObstacleQuantity
    {
        LESS = 5,
        NORMAL = 13,
        MORE = 20
    }

    public enum CaseType
    {
        EMPTY,
        STONE,
        TREE,
        WATER,
        OTHER
    }


    public class RandomMap
    {
        private CaseType[,] Map;
        private Pair size;

        private Pair chooseStartingPoint()
        {
            for (uint i = 0; i < size.First; ++i)
            {
                for (uint j = 0; j < size.Second; ++j)
                {
                    if (Map[i, j] == CaseType.EMPTY)
                        return new Pair(i, j);
                }
            }
            return new Pair(size.First, size.Second);
        }

        private bool isMapValid()
        {
            bool[,] vect = new bool[size.First, size.Second];
            Stack<Pair> stack = new Stack<Pair>();

            // init reach vector
            for (int i = 0; i < size.First; ++i)
            {
                for (int j = 0; j < size.Second; ++j)
                {
                    vect[i, j] = false;
                }
            }

            Pair Start = chooseStartingPoint();
            if (Start.First == size.First)
                return false; // Aucune case vide

            stack.Push(Start);
            while (stack.Count > 0)
            {
                Pair p = stack.Pop();
                vect[p.First, p.Second] = true;

                // Tests des cases adjacentes
                if (p.First < size.First - 1)
                {
                    if (Map[p.First + 1, p.Second] == CaseType.EMPTY)
                    {
                        if (!vect[p.First + 1, p.Second])
                            stack.Push(new Pair(p.First + 1, p.Second));
                    }
                    else
                        vect[p.First + 1, p.Second] = true;
                }

                if (p.Second < size.Second - 1)
                {
                    if (Map[p.First, p.Second + 1] == CaseType.EMPTY)
                    {
                        if (!vect[p.First, p.Second + 1])
                            stack.Push(new Pair(p.First, p.Second + 1));
                    }
                    else
                        vect[p.First, p.Second + 1] = true;
                }

                if (p.First > 0)
                {
                    if (Map[p.First - 1, p.Second] == CaseType.EMPTY)
                    {
                        if (!vect[p.First - 1, p.Second])
                            stack.Push(new Pair(p.First - 1, p.Second));
                    }
                    else
                        vect[p.First - 1, p.Second] = true;
                }

                if (p.Second > 0)
                {
                    if (Map[p.First, p.Second - 1] == CaseType.EMPTY)
                    {
                        if (!vect[p.First, p.Second - 1])
                            stack.Push(new Pair(p.First, p.Second - 1));
                    }
                    else
                        vect[p.First, p.Second - 1] = true;
                }
            }

            for (int i = 0; i < size.First; ++i)
                for (int j = 0; j < size.Second; ++j)
                {
                    if (!vect[i, j])
                        return false;
                }

            return true;
        }

        public void afficherMapConsole()
        {
            Console.Write("|");
            for (int i = 0; i < size.First; ++i)
            {
                for (int j = 0; j < size.Second; ++j)
                {
                    string s = " ";
                    if (Map[i, j] == CaseType.EMPTY)
                        s += " ";
                    else if (Map[i, j] == CaseType.STONE)
                        s += "S";
                    else if (Map[i, j] == CaseType.TREE)
                        s += "T";
                    s += " |";
                    Console.Write(s);
                }
                Console.Write("\n|");
            }
        }

        public static RandomMap GenerateMap(GroundType type, ObstacleQuantity quantity, Pair NbCases)
        {
            RandomMap myMap = new RandomMap();
            myMap.Map = new CaseType[NbCases.First, NbCases.Second];
            myMap.size = NbCases;

            // Initialize empty Map
            for (int i = 0; i < NbCases.First; ++i)
            {
                for (int j = 0; j < NbCases.Second; ++j)
                {
                    myMap.Map[i, j] = CaseType.EMPTY;
                }
            }

            // Calculatate amount of stones and trees
            uint NbStones = 0;
            uint NbTrees = 0;
            uint NbWater = 0;
            uint NbOther = 0;
            uint size = myMap.size.First * myMap.size.Second;
            switch (type)
            {
                case GroundType.WOODY:
                    NbStones = (uint)(size * ((float)quantity / 100) * 0.15);
                    NbTrees = (uint)(size * ((float)quantity / 100) * 0.60);
                    NbOther = (uint)(size * ((float)quantity / 100) * 0.10);
                    NbWater = (uint)(size * ((float)quantity / 100) * 0.15);
                    break;
                case GroundType.ROCKY:
                    NbStones = (uint)(size * ((float)quantity / 100) * 0.60);
                    NbTrees = (uint)(size * ((float)quantity / 100) * 0.10);
                    NbOther = (uint)(size * ((float)quantity / 100) * 0.15);
                    NbWater = (uint)(size * ((float)quantity / 100) * 0.15);
                    break;
                case GroundType.WATERY:
                    NbStones = (uint)(size * ((float)quantity / 100) * 0.15);
                    NbTrees = (uint)(size * ((float)quantity / 100) * 0.10);
                    NbOther = (uint)(size * ((float)quantity / 100) * 0.15);
                    NbWater = (uint)(size * ((float)quantity / 100) * 0.60);
                    break;
                default:
                    NbStones = (uint)(size * ((float)quantity / 100) * 0.25);
                    NbTrees = (uint)(size * ((float)quantity / 100) * 0.30);
                    NbOther = (uint)(size * ((float)quantity / 100) * 0.20);
                    NbWater = (uint)(size * ((float)quantity / 100) * 0.25);
                    break;
            }

            // Generate put and obstacles on the map
            Random randomL = new Random();
            for (; NbStones > 0; --NbStones)
            {
                int rl = randomL.Next(0, (int)NbCases.First);
                int rc = randomL.Next(0, (int)NbCases.Second);

                if (myMap.Map[rl, rc] == CaseType.EMPTY)
                {
                    myMap.Map[rl, rc] = CaseType.STONE;
                }
                else
                    ++NbStones;
            }
            for (; NbTrees > 0; --NbTrees)
            {
                int rl = randomL.Next(0, (int)NbCases.First);
                int rc = randomL.Next(0, (int)NbCases.Second);

                if (myMap.Map[rl, rc] == CaseType.EMPTY)
                {
                    myMap.Map[rl, rc] = CaseType.TREE;
                }
                else
                    ++NbTrees;
            }
            for (; NbWater > 0; --NbWater)
            {
                int rl = randomL.Next(0, (int)NbCases.First);
                int rc = randomL.Next(0, (int)NbCases.Second);

                if (myMap.Map[rl, rc] == CaseType.EMPTY)
                {
                    myMap.Map[rl, rc] = CaseType.WATER;
                }
                else
                    ++NbTrees;
            }
            for (; NbOther > 0; --NbOther)
            {
                int rl = randomL.Next(0, (int)NbCases.First);
                int rc = randomL.Next(0, (int)NbCases.Second);

                if (myMap.Map[rl, rc] == CaseType.EMPTY)
                {
                    myMap.Map[rl, rc] = CaseType.OTHER;
                }
                else
                    ++NbTrees;
            }

            if (myMap.isMapValid())
            {
                // Uncomment for debug
                //myMap.afficherMapConsole();
                return myMap;
            }
            else
                return GenerateMap(type, quantity, NbCases);
        }

        public List<TileContainer> ConvertToServerMap()
        {
            List<TileContainer> ServerMap = new List<TileContainer>();
            for (int i = 0; i < size.First; ++i)
            {
                for (int j = 0; j < size.Second; ++j)
                {
                    TileContainer tile = new TileContainer() { Col = i, Line = j };
                    ServerMap.Add(tile);
                    switch (Map[i, j])
                    {
                        case CaseType.STONE:
                            tile.Name = "Stone";
                            break;
                        case CaseType.TREE:
                            tile.Name = "Tree";
                            break;
                        case CaseType.WATER:
                            tile.Name = "Water";
                            break;
                        case CaseType.OTHER:
                            tile.Name = "Other";
                            break;
                        default:
                            break;
                    }
                }
            }

            return ServerMap;
        }

        // For debug purpose only
        public void DisplayServerMap()
        {
            List<TileContainer> list = this.ConvertToServerMap();
            foreach (var terrain in list)
            {
                Console.Write(terrain.Name + "(line: " + terrain.Line + ", col: " + terrain.Col + ")\n");
            }
        }
    }
}
