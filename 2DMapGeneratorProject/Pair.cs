﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MapGeneratorLibrary
{
    // La classe KeyValuePair<K, V> existe =D
    public class Pair
    {
        public Pair()
        {
        }

        public Pair(uint first, uint second)
        {
            this.First = first;
            this.Second = second;
        }

        public uint First { get; set; }
        public uint Second { get; set; }
    };
}
