﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

	public 	float Speed;
	private float _speedCoeff = 0.001f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(transform.position.x + Speed*_speedCoeff, transform.position.y, transform.position.z);
	}
}
