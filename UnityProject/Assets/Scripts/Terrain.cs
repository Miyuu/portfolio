﻿using UnityEngine;
using System.Collections;

public class Tile{
	private 		Vector3 	m_pos; // scene coordinates
	public			Vector3 	Pos 	{ 	get{
												return m_pos;
											}
											set{
												m_pos = value;
											}
										}
	public			GameObject	Data;
}
	
public class Terrain : MonoBehaviour {
	public 	static 	uint 	BOARD_MAX_SIZE	=(15*20); //hxw
	public 			Tile[]	Board 			{get; set;}

	private 		Vector3 m_pos; // scene coordinates
	public			Vector3 Pos 			{	get{
													return m_pos;
												}
											}			
	
	// instance
	private	static 	Terrain	m_instance = null;
	public 	static 	Terrain Instance{	get{
											if (m_instance == null){
												GameObject go = new GameObject();
												m_instance = go.AddComponent<Terrain>();
											}
											m_instance.Initialize();
											return m_instance;
										}
									}

	// Use this for initialization
	void Start () {
		m_instance = Instance;
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnMouseUp(){
		Debug.Log("clicked!");
	}

	void Initialize(){
		m_pos = new Vector3 (-8, 5, 0);
		Board = new Tile[BOARD_MAX_SIZE];
		for (uint count=0; count<BOARD_MAX_SIZE; count++) {
			Board[count] = new Tile();
			Board[count].Pos = new Vector3(Pos.x + (count%20)*1.25f, 
			                    Pos.y + (count/20)*1.25f, 
			                    Pos.z);
		}
	}

	Tile GetTileAtIndex(uint index){
		if (index < BOARD_MAX_SIZE){
			return Board[index];
		}
		else {
			throw new System.IndexOutOfRangeException();
		}
	}

	void SetTileAtIndex(Tile tile, uint index){
		if (index < BOARD_MAX_SIZE){
			Board[index] = tile;
		}
		else {
			throw new System.IndexOutOfRangeException();
		}
	}
}
