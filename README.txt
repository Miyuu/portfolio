##########################################
##    
                 PORTFOLIO                        
##
##########################################



There are 3 project samples in this portfolio. All are uni work (1 or 2 years old), since French laws prohibits sharing of company code (it belongs to the company, not the programmer).

Below is a quick explanation of the projects.



## 2DMapGenerator

This code sample was part of a bigger project for Epita. It generates a 2D terrain for a tile-based strategy game (for instance fire emblem). The idea was to be able to generate random maps for our game with more or less obstacles and different terrain type.

Programmed in C#.



## BehaviourTree

The behaviour tree was a project for AI course at Staffordshire University. It is a basic tree-based decision-making system for tile-based strategy game. Each unit on the board decides the best action to perform depending on its remaining life and nearby enemies.

Programmed in C++.



## SteinsEngine

Samples of a basic game engine project for Staffordshire University. The principle was to develop a component-based generic engine with high independence and reusability. Components such as core engine, AI or physics and game objects can communicate via the message center.

Programmed in C++.

## UnityProject
A simple grid-based RTS written with Unity 4.3 to get my hands around its 2D tools.I had worked on 4.2 based Unity projects and want to extend my knowledge ont his software, as well as improve my C# abilities.




#
If you have any inquiries about these code samples, please email me at tiffany.berenguer@gmail.com

Thank you for your attention.